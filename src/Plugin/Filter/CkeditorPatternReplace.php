<?php

namespace Drupal\ckeditor_pattern_replace\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Filter process to replace a regular expression pattern.
 *
 * @Filter(
 *   id = "ckeditor_pattern_replace",
 *   title = @Translation("CKEditor pattern replacement"),
 *   description = @Translation("Helps to replace a pattern in CKEditor field."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_HTML_RESTRICTOR,
 * )
 */
class CkeditorPatternReplace extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $ckeditor_pattern_replace = $this->settings['ckeditor_pattern_replace'];
    if (isset($ckeditor_pattern_replace)) {
      $pattern_replace = explode("\n", trim($ckeditor_pattern_replace));
      foreach ($pattern_replace as $value) {
        $element = explode("|", trim($value));
        $text = preg_replace($element[0], $element[1], $text);
      }
    }
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['ckeditor_pattern_replace'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add replacement patterns'),
      '#default_value' => $this->settings['ckeditor_pattern_replace'],
      '#placeholder' => $this->t('/RegularExpression/|Replacement'),
      '#description' => $this->t(
        'Enter one value per line, in the format pattern|replacement.
        E.g. /RegularExpression/|Replacement.'
      ),
    ];
    return $form;
  }

}
