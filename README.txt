CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

CKEditor pattern replace module is designed to provide an option at 
"Text formats and editors" to filter the pattern using regular expression 
and replace it with replacement text.


REQUIREMENTS
------------
* No external library's dependency.
* Core's CKEditor module is required to be enabled.

INSTALLATION
------------

* Put the module in your Drupal modules directory and enable it in
  admin/modules.
* Using Drush :: drush pm-enable ckeditor_pattern_replace

CONFIGURATION
-------------

* Configure settings can be found at:
 /admin/config/content/formats/manage/<CKEditor_Format>.
* Under the section of "Enabled filters", check the option of:
 "CKEditor pattern replacement".
* Then you would get an option at "Filter settings" section to add
  replacement pattern.

MAINTAINERS
-----------

Current maintainer(s):
 * Neeraj Singh (neerajsingh) - https://www.drupal.org/u/neerajsingh
